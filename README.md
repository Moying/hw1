first.m is the code for the first problem
The user can run directly
Input the points numbers n and get three plots.

#################################################

second.m is the code for the second problem
The user can run directly
Input the precision and get the steps that are required for each level of precision.

#################################################

third.m is the code for the third problem
For example, the user can input “third(3)” in the command line to call it to get the result for precision 3
then the graph and the computed pi is given.
